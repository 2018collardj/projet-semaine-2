from django import forms
from django.utils import timezone
from .models import *

class ContactForm(forms.Form):
    sujet = forms.CharField(max_length=100)
    message = forms.CharField(widget=forms.Textarea)
    envoyeur = forms.EmailField(label="Votre adresse e-mail")
    renvoi = forms.BooleanField(help_text="Cochez si vous souhaitez obtenir une copie du mail envoyé.", required=False)

class NouveauUtilisateurForm(forms.Form):
    nom = forms.CharField()
    adresse = forms.CharField(widget=forms.Textarea)
   
class NouvelleRecetteForm(forms.Form):
    nom =forms.CharField(max_length=30)
    date = forms.DateTimeField(required=False)
    categorie = forms.ModelMultipleChoiceField(queryset=Categorie.objects.all(), required=False)
    difficulte = forms.CharField(required=False)
    temps = forms.TimeField(required=False)
    ingredient = forms.ModelMultipleChoiceField(queryset=Ingredient.objects.all(), required=False)
    ustensile = forms.ModelMultipleChoiceField(queryset=Ustensile.objects.all(), required=False)
    calorie = forms.CharField(required=False)
    descriptif = forms.CharField(required=False)
