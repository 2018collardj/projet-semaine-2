from django.test import TestCase
from datetime import datetime, timedelta
from django.urls import resolve, reverse
from django.test import Client
from .models import *
from .views import *

#coverage run --source='.' manage.py test app
#coverage report

class RecetteTests(TestCase):
    #Test du formulaire django
    # Formulaire Valide
    def test_NouvelleRecetteForm_valid(self):
        form = NouvelleRecetteForm(data={'nom':'defaut'})
        self.assertTrue(form.is_valid())

    # Formulaire Invalide
    def test_NouvelleRecetteForm_invalid(self):
        form = NouvelleRecetteForm(data={'nom': None})
        self.assertFalse(form.is_valid())

    #Création d'un nouvel ingredient à partir d'une recette
    def test_NouvelIngredient(self):
        #Ajout d'un ingredient déjà existant à une recette
        ing = Ingredient.objects.create(nom='def_ing')
        rec = Recette.objects.create(nom='def_rec')
        rec.ingredient.add(ing)
        #Création d'un ingredient en l'ajoutant à une recette
        rec.ingredient.create(nom='def_ing2')
        item = Ingredient.objects.get(nom='def_ing2')
        self.fail="Creation de la recette échoue"

"""class ContactFormTests(TestCase):
    #Test du formulaire django
    # Formulaire Valide
    def test_ContactForm_valid(self):
        form = ContactForm(data={'sujet' : "Sujet", 'message':"Message", 'envoyeur':"adresse@hote.fr", 'renvoi':True})
        self.assertTrue(form.is_valid())
    # Formulaire Invalide
    def test_ContactForm_valid(self):
        form = ContactForm(data={'sujet' : "", 'message':"Message", 'envoyeur':"adresse@hote.fr", 'renvoi':True})
        self.assertFalse(form.is_valid())"""

class UrlsTests(TestCase):
    
    def test_views_resolved(self):
        self.assertEqual(resolve('/app/test').view_name, 'test')
        self.assertEqual(resolve('/app/accueil').view_name, 'accueil')
        self.assertEqual(resolve('/app/contact').view_name, 'contact')

class ViewsTests(TestCase):

    def test_urls_reversed(self):
        self.assertEqual(reverse('traitement'), '/app/traitement')

class TraitementTest(TestCase):
    #Test du formulaire html
    def test_traitement_defaut(self):
        cat = Categorie.objects.create(nom='Moyen')
        ing = Ingredient.objects.create(nom='def_ing')
        rec = Recette.objects.create(nom='def_rec')
        rec.ingredient.add(ing)
        rec.categorie.add(cat)

        client = Client()
        response = self.client.post('/app/traitement', {
            'nom_recette': rec.nom,
            'categories': rec.categorie,
            'ingredients_choisis':rec.ingredient,
            'difficulte': rec.difficulte,
            'calories': rec.calorie
        }, follow=True)

        self.assertEqual(response.status_code, 200)

"""class ContactTest(TestCase):
    #Test du formulaire django
    def test_traitement_defaut(self):
        client = Client()
        response = self.client.post('/app/contact', {
            'sujet': 'sujet',
            'message': 'message',
            'envoyeur': 'adresse@hote.fr',
            'renvoi': 'renvoi'
        }, follow=True)

        self.assertEqual(response.status_code, 200)"""
