from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('accueil', views.accueil, name="accueil"),
    path('contact', views.contact, name="contact"),
    path('test', views.test, name='test'),
    path('traitement', views.traitement, name='traitement')
]