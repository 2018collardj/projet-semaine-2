from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, Http404
from datetime import datetime
from .forms import *
from app import models
from .models import *
# Create your views here.

def accueil(request):
    return render(request, 'main.html')
    
def contact(request):
    # Construire le formulaire, soit avec les données postées,
    # soit vide si l'utilisateur accède pour la première fois
    # à la page.

    form = ContactForm(request.POST or None)

    # Nous vérifions que les données envoyées sont valides
    # Cette méthode renvoie False s'il n'y a pas de données 
    # dans le formulaire ou qu'il contient des erreurs.

    if form.is_valid(): 
        # Ici nous pouvons traiter les données du formulaire
        sujet = form.cleaned_data['sujet']
        message = form.cleaned_data['message']
        envoyeur = form.cleaned_data['envoyeur']
        renvoi = form.cleaned_data['renvoi']


        # Nous pourrions ici envoyer l'e-mail grâce aux données 
        # que nous venons de récupérer
        envoi = True

    # Quoiqu'il arrive, on affiche la page du formulaire.
    return render(request, 'app/contact.html', locals())

def nouveau_contact(request):
    sauvegarde = False
    form = NouveauUtilisateurForm(request.POST or None, request.FILES)

    if form.is_valid():
        contact = Contact()
        contact.nom = form.cleaned_data["nom"]
        contact.adresse = form.cleaned_data["adresse"]
        contact.save()
        sauvegarde = True

    return render(request, 'nouveau_contact.html', {
        'form': form, 
        'sauvegarde': sauvegarde
    })

def test(request):
    dico={"categorie":Categorie.objects.all(),"ingredient":Ingredient.objects.all()}
    return HttpResponse(render(request,"app/Finder.html",dico))

def traitement(request):
    #Recuperation des données du formulaire html
    name=request.POST.getlist('nom_recette')[0]
    categories=request.POST.getlist('categorie')
    categorie_logique=request.POST.getlist('categorie_logique')
    ingredients_choisis=request.POST.getlist('ingredient')
    allergenes=request.POST.getlist('allergene')
    difficulte=request.POST.getlist('difficulte')[0]
    calories=request.POST.getlist('calories')[0]

    #Filtrage
    resultat=[]
    liste_categorie = Categorie.objects.all().distinct()
    filtre = Recette.objects.filter(categorie__in=liste_categorie).distinct().order_by('nom')

    if categories !=  ["none"]:
        liste_categorie = liste_categorie.filter(nom__in = categories) 

    if ingredients_choisis != ["none"]:
        liste_ingredient = Ingredient.objects.filter(nom__in = ingredients_choisis)
        filtre = filtre.filter(ingredient__in=liste_ingredient)
    if allergenes != ["none"]:
        liste_allergenes = Ingredient.objects.filter(nom__in = allergenes)
        filtre = filtre.exclude(ingredient__in=liste_allergenes)
         


    if name != "" :
        filtre = filtre.filter(nom__contains = name)
    if difficulte != "none":
        filtre = filtre.filter(difficulte = difficulte)
    if calories != "none":
        filtre = filtre.filter(calorie = calories)

    liste_RecetteIngredient = []
    for rec in filtre :
        ings = []
        for ing in rec.ingredient.all():
            ings += [ing.nom] 
        liste_RecetteIngredient += [(rec.nom, ings, rec.descriptif)]    

    return render(request, 'app/Finder.html', {'recette' : liste_RecetteIngredient, "categorie":Categorie.objects.all(),"ingredient":Ingredient.objects.all()})