from django.db import models
from django.utils import timezone
           


class Categorie(models.Model):
    nom = models.CharField(max_length=30)

    def __str__(self):
        return self.nom

class Ingredient(models.Model):
    nom = models.CharField(max_length=100)
    allergene = models.BooleanField(default=False)
    prix = models.CharField(max_length=100, default='Moyen')

    def __str__(self):
        return self.nom


class Ustensile(models.Model):
    nom = models.CharField(max_length=100)

    def __str__(self):
        return self.nom

class Recette(models.Model):
    nom = models.CharField(max_length=100)
    date = models.DateTimeField(default=timezone.now, verbose_name="Date de parution")
    categorie = models.ManyToManyField(Categorie, related_name="recette")
    difficulte = models.CharField(max_length=100, default='Moyen')
    temps = models.CharField(max_length=100, default='Moyen')
    ingredient = models.ManyToManyField(Ingredient, related_name="recette")
    descriptif = models.TextField(null=True)
    ustensile = models.ManyToManyField(Ustensile, related_name="recette")
    calorie = models.CharField(max_length=100)

    class Meta:
        verbose_name = "recette"
        ordering = ['nom']

    def __str__(self):
        return self.nom

class Utilisateur(models.Model):
    nom = models.CharField(max_length=255)
    adresse = models.TextField()
    #favoris = models.ManyToManyField(Recette, related_name="Utilisateur")

    def __str__(self):
           return self.nom

class Commentaire(models.Model):
    #auteur = models.ForeignKey(Utilisateur, on_delete=models.CASCADE)
    contenu = models.TextField(null=True)
    date = models.DateField(default=timezone.now)