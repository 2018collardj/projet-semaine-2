from django.contrib import admin
from django.utils.text import Truncator
from .models import *

class RecetteAdmin(admin.ModelAdmin):
    list_display   = ('nom', 'date', 'apercu_ingredients')
    list_filter    = ('categorie','ingredient')
    ordering       = ('nom', )
    search_fields  = ('nom', 'ingredient')
    
    #fields = ('contenu',)  attributs modifiables
  
    fieldsets = (

    # Fieldset 1 : meta-info (titre, auteur…)
    ('Général', {
        'classes': ['collapse',],
        'fields': ('nom', 'categorie')
    }),

    # Fieldset 2 : contenu de l'article
    ('Contenu de l\'article', {
       'description': 'Le formulaire accepte les balises HTML. Utilisez-les à bon escient !',
       'fields': ('ingredient', 'descriptif' )
    }),
) 
    def apercu_ingredients(self, recette):
        return Truncator(recette.descriptif).chars(40, truncate='...')

    # En-tête de notre colonne
    apercu_ingredients.short_description = 'Aperçu du contenu'
    
admin.site.register(Categorie)
admin.site.register(Ingredient)
admin.site.register(Ustensile)
admin.site.register(Recette, RecetteAdmin)
